# Dankie Bot


## Installation

    $ git clone git@gitlab.com:lukeovalle/dankiebot.git
    $ cd dankiebot

And then execute:

    $ bundle install


## Usage

Copy config-sample.yml to config.yml and put your API token in there
It's required to have a Redis installation running in the port 42069, or if in
other port, set the corresponding port in config.yml

the bot runs with the following command

    $ ruby bot.rb


## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/lukeovalle/dankiebot.


## License

see LICENSE
